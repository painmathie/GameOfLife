unit HerbeProg;
interface
Uses Crt, Utils;

Function InitialiserGeneration(tableau : TabPosition) : TypeGeneration;
Procedure AfficherGeneration(grille : TypeGeneration);
Procedure WriteGeneration(grille : TypeGeneration; var fic2 : Text);
Function Run(grille : TypeGeneration) : TypeGeneration;
Function RunGeneration(grille : TypeGeneration; nombreIteration : Integer) : TypeGeneration;
Function InitialiserGenerationRandom(nb : Integer) : TypeGeneration;

implementation

Function InitialiserGenerationRandom(nb : Integer) : TypeGeneration;
	Var i, j, r: Integer;
		grille : TypeGeneration;
		varHerbe : Herbe;
	Begin
		initialiserTableau(grille);
		For i := 0 To N-1 Do
			For j := 0 To N-1 Do Begin
				r := Random(100);
				If r <= nb Then begin
					varHerbe.energie := HERBE_ENERGIE_INITIALE;
					varHerbe.age := 0;
					varHerbe.alive := True;
					grille[i][j].herbep := varHerbe;
				end;
			end;
		InitialiserGenerationRandom := grille;
	End;
// génération aléatoire de la grille de l'herbe

Function InitialiserGeneration(tableau : TabPosition) : TypeGeneration;
	Var i, x, y : Integer;
		grille : TypeGeneration;
		varHerbe : Herbe;
	Begin
		initialiserTableau(grille);
		For i := 1 To M Do Begin
			x := tableau[i].x;
			y := tableau[i].y;
			If (x <> -1) And (y <> -1) Then Begin
				varHerbe.energie := HERBE_ENERGIE_INITIALE;
				varHerbe.age := 0;
				varHerbe.alive := True;
				grille[x][y].herbep := varHerbe;
			End;
		End;
		InitialiserGeneration := grille;
	End;
//génération manuel de la grille de l'herbe

Procedure AfficherGeneration(grille : TypeGeneration);
	Var i,j : integer;	
	Begin
		For i := 0 To N-1 Do Begin
			For j := 0 To N-1 Do Begin
				If grille[i][j].herbep.alive = True Then
					TextBackground(Green);
				write('  ');
				TextBackground(Black);
			End;
				WriteLn('|');
		End;
		For j := 0 To N-1 Do
			write('--');
		writeln('+');
	End;
// affiche un génération sur un écran

Procedure WriteGeneration(grille : TypeGeneration; var fic2 : Text);
	Var i,j : integer;	
	Begin
		For i := 0 To N-1 Do Begin
			write(fic2, '#');
			For j := 0 To N-1 Do Begin
				If grille[i][j].herbep.alive = False Then 
					Write (fic2, ' ')
				Else Write (fic2, '#');	
			End;
			Writeln(fic2, '');
		End;

		saveGrille(fic2, grille);
	End;

// affiche la génération sur un programme
	
Function Run(grille : TypeGeneration) : TypeGeneration;
	Var i, j : integer;
		herbe2: Herbe;
		grille2 : typeGeneration;
	Begin
		initialiserTableau(grille2);
		For i := 0 To N-1 Do Begin
			For j := 0 To N-1 Do Begin
				herbe2 :=grille[i][j].herbep;
				If herbe2.alive = True Then Begin
					If herbe2.age < HERBE_AGE_MAX Then Begin
						herbe2.age := herbe2.age + 1;
						herbe2.energie := herbe2.energie + HERBE_ENERGIE; 
						If herbe2.energie >= HERBE_ENERGIE_REPRODUCTION Then Begin
							reproductionherbe(i,j, herbe2,grille, grille2);
							herbe2.energie := herbe2.energie - HERBE_ENERGIE_REPRODUCTION;
						End;
					End
					Else Begin
						herbe2.age := 0;
						herbe2.energie := 0;
						herbe2.alive := False;
					End;
					grille2[i][j].herbep := herbe2;
				End;					 
			End;
		End;
		Run := grille2;
	End;
// boucle principale

Function CompteHerbe(grille : TypeGeneration) : Integer;
Var i,j, r : Integer;
Begin
	r := 0;
	For i := 0 To N-1 Do
		For j := 0 To N-1 Do
			If grille[i][j].herbep.alive Then
				r := r + 1;
	CompteHerbe := r;
End;
Function RunGeneration(grille : TypeGeneration; nombreIteration : Integer) : TypeGeneration;
	Var i : integer;
	Begin
		ClrScr;
		If nombreIteration > 0 Then begin
			AfficherGeneration(grille);
			Delay(1000);
			For i := 1 To nombreIteration Do Begin
				grille := Run(grille);
				ClrScr;
			End;
			AfficherGeneration(grille);
			WriteLn(CompteHerbe(grille), ' herbes vivantes.');
		end
		Else Begin
			Repeat
				grille := Run(grille);
				Delay(100);
				ClrScr;
				AfficherGeneration(grille);
				WriteLn(CompteHerbe(grille), ' herbes vivantes.');
			Until KeyPressed;
		End;
		RunGeneration := grille
	End;
// lance la simulation

End.

