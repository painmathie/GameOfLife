Program Main;
Uses Crt, GameOfLifeProg, HerbeProg, MoutonProg, LoupProg, Utils;


Procedure getFile(i : Integer; Var file1, file2 : String);
	Begin
	If ParamStr(i) = '-o' Then
		file1 := ParamStr(i+1)
	Else If ParamStr(i) = '-i' Then
		file2 := ParamStr(i+1);
	End;


function getList(list : String) : tabPosition;
	Var
		tab : tabPosition;
		typepos : TypePosition;
		i, min, max, step, e, j : Integer;
		text : String;
	Begin
		For i:=1 To M Do Begin
			typepos.x := -1;
			typepos.y := -1;
			tab[i] := typepos;
		End;
		min := Pos('[', list);
		max := Pos(']', list);
		step := 0;
		j := 1;
		e := 0;
		text := '';
		For i := min+1 To max-1 Do
			If (list[i] = '(') And (step = 0) Then
				step := 1
			Else If (list[i] = ' ') And (step = 1) Then Begin
				Val(text, typepos.x, e);
				text := '';
				step := 2;
			End Else If (list[i] = ')') And (step = 2) Then Begin
				step := 0;
				Val(text, typepos.y, e);
				If (typepos.x >= N) Or (typepos.x < -1) Or (typepos.x >= N) Or (typepos.x < -1) Then Begin
					WriteLn('Erreur, coordonées non compris entre -1 (par défault) et ', N);
					halt;				
				End Else Begin
					tab[j] := typepos;
					j := j+1;				
				End;
				text := '';
			End Else 
				text := text + list[i];
			If e <> 0 Then Begin
				WriteLn('Erreur dans les coordonnées !');
				halt;
			End;
		getList := tab;
	End;

Function readGeneration(Var fic : Text; Var grille : TypeGeneration) : Boolean;
Var x, y, i, e, state : Integer;
	start, havefind : Boolean;
	ligne, ligne2 : String;
	varHerbe : Herbe;
	varMouton : Mouton;
	varLoup : Loup;
Begin
	x := 0;
	y := 0;
	e := 0;
	start := False;
	havefind := False;
	reset(fic);
	ligne2 := '';
	state := 0;
	Repeat
		ReadLn(fic,ligne);
		For i := 1 To Length(ligne) Do Begin
			If start Then Begin
				havefind := True;
				if (ligne[i] <> '$') and (ligne[i] <> '*') and (ligne[i] <> ':') and (ligne[i] <> ';') Then
					ligne2 := ligne2 + ligne[i]
				Else If (ligne[i] = '$') or (ligne[i] = '*') or (ligne[i] = ':') or (ligne[i] = ';') Then Begin
					If (state = 0) And (ligne[i] = ';') Then Begin
						state := 1;
						varHerbe.alive := ligne2 = 'TRUE';
						ligne2 := '';
					End Else If (state = 1) And (ligne[i] = ';') Then Begin
						state := 2;
						Val(ligne2, varHerbe.age, e);
						ligne2 := '';
					End Else If (state = 2) And (ligne[i] = ':') Then Begin
						state := 3;
						Val(ligne2, varHerbe.energie, e);
						grille[y][x].herbep := varHerbe;
						ligne2 := '';
					End Else If (state = 3) And (ligne[i] = ';') Then Begin
						state := 4;
						varMouton.alive := ligne2 = 'TRUE';
						ligne2 := '';
					End Else If (state = 4) And (ligne[i] = ';') Then Begin
						state := 5;
						Val(ligne2, varMouton.age, e);
						ligne2 := '';
					End Else If (state = 5) And (ligne[i] = ':') Then Begin
						state := 6;
						Val(ligne2, varMouton.energie, e);
						grille[y][x].moutonp := varMouton;
						ligne2 := '';
					End Else If (state = 6) And (ligne[i] = ';') Then Begin
						state := 7;
						varLoup.alive := ligne2 = 'TRUE';
						ligne2 := '';
					End Else If (state = 7) And (ligne[i] = ';') Then Begin
						state := 8;
						Val(ligne2, varLoup.age, e);
						ligne2 := '';
					End Else If (state = 8) And (ligne[i] = '*') Then Begin
						state := 0;
						Val(ligne2, varLoup.energie, e);
						grille[y][x].loupp := varLoup;
						ligne2 := '';

						x := x + 1;
						If x >= N Then Begin
							y := y + 1;
							x := 0;
						End;
					End;  
				End;
			End;
			If e <> 0 Then Begin
				WriteLn('Erreur dans la lecture du fichier');
				halt;				
			End;
			If (ligne[i] = '$') Then
				start := start = False;
		End;
	Until eof(fic);
	If havefind Then
		readGeneration := havefind;
End;

Function readGrille(Var fic : Text; Var grille : TypeGrille) : Boolean;
Var x, y, i, e : Integer;
	start, havefind : Boolean;
	ligne : String;
Begin
	x := 0;
	y := 0;
	e := 0;
	start := False;
	havefind := False;
	reset(fic);
	Repeat
		ReadLn(fic,ligne);
		For i := 1 To Length(ligne) Do Begin
			If start Then Begin
				havefind := True;
				If ligne[i] = ';' Then Begin
				x := x + 1;
				If x >= N Then Begin
					y := y + 1;
					x := 0;
				End;
			End Else If ligne[i] <> '$' Then Val(ligne[i], grille[y][x], e);
			If e <> 0 Then Begin
				WriteLn('Erreur dans la lecture du fichier');
				halt;				
			End;	
			End;
			If (ligne[i] = '$') Then
				start := start = False;
		End;
	Until eof(fic);
	If havefind Then
	AfficherGrille(grille);
	readGrille := havefind;
End;


Procedure GameOfLifeInit(Var grille : TypeGrille; israndom : Boolean; position : tabPosition; fileinput : String; Var fic : Text);
var i : Integer;
begin
	If fileinput <> '' Then
		if (readGrille(fic, grille)) Then
			exit();
	If israndom Then Begin
		WriteLn('Remplir avec N pourcent :');
		ReadLn(i);
		grille := GameOfLifeProg.InitGrille(i);
	End Else 
		grille := GameOfLifeProg.RemplirGrille(position);
end;

Procedure RunGameOfLife(israndom : Boolean; position : tabPosition; nombreGeneration : Integer; fileoutput : String; Var fic2 : Text; fileinput : String; Var fic : Text);
	Var grille : TypeGrille;
		e, i : Integer;
		stop, first : Boolean;
	Begin	
		stop := False;
		GameOfLifeInit(grille, israndom, position, fileinput, fic);
		first := fileinput = '';
		fileinput := '';
		Repeat
			If first Then
				grille := GameOfLifeProg.Run(grille, nombreGeneration)
			Else Begin first := True;
				GameOfLifeProg.afficherGrille(grille);
			End;
			WriteLn('Que voullez vous faire :');
			WriteLn('1 : Relancer la simulation');
			If nombreGeneration < 0 Then
				WriteLn('2 : Poursuivre');
			WriteLn('0 : Quitter');
			e := 0;
			Repeat
				ReadLn(i);
				If i = 1 Then Begin
					GameOfLifeInit(grille, israndom, position, fileinput, fic);
					e := 1;
				End Else If (i = 2) And (nombreGeneration < 0) Then Begin
					e := 1;
				End Else Begin
					e := 1;
					stop := True;
				End;
			Until e <> 0;
		Until stop <> False;
		If fileoutput <> '' Then Begin
			WriteLn(fic2, '#Last Simulation : ');
			GameOfLifeProg.WriteGrille(grille, fic2);
		End;
	End;

Procedure HerbeInit(Var grille : TypeGeneration; israndom : Boolean; position : tabPosition; fileinput : String; Var fic : Text);
	Var i : Integer;
	Begin
	
		If fileinput <> '' Then
			if (readGeneration(fic, grille)) Then
				exit();
		If israndom Then Begin
			WriteLn('Remplir avec N pourcent :');
			ReadLn(i);
			grille := HerbeProg.InitialiserGenerationRandom(i);
		End Else 
			grille := HerbeProg.InitialiserGeneration(position);
	End;

Procedure RunHerbe(israndom : Boolean; position : tabPosition; nombreGeneration : Integer; fileoutput : String;Var fic2 : Text; fileinput : String; Var fic : Text);
	Var grille : TypeGeneration;
		e, i : Integer;
		stop, first : Boolean;
	Begin	
		stop := False;
		HerbeInit(grille, israndom, position, fileinput, fic);
		first := fileinput = '';
		fileinput := '';
		Repeat
			If first Then
				grille := HerbeProg.RunGeneration(grille, nombreGeneration)
			Else Begin first := True;
				HerbeProg.afficherGeneration(grille);
			End;
			WriteLn('Que voullez vous faire :');
			WriteLn('1 : Relancer la simulation');
			If nombreGeneration < 0 Then
				WriteLn('2 : Poursuivre');
			WriteLn('0 : Quitter');
			e := 0;
			Repeat
				ReadLn(i);
				If i = 1 Then Begin
					HerbeInit(grille, israndom, position, fileinput, fic);
					e := 1;
				End Else If (i = 2) And (nombreGeneration < 0) Then Begin
					e := 1;
				End Else Begin
				e := 1;
					stop := True;
				End;
			Until e <> 0;
		Until stop <> False;
		If fileoutput <> '' Then Begin
			WriteLn(fic2, '#Last Simulation : ');
			HerbeProg.WriteGeneration(grille, fic2);
		End;
	End;

Procedure MoutonInit(Var grille : TypeGeneration; israndom : Boolean; position, position2 : tabPosition; fileinput : String; Var fic : Text);
	Var i,j  : Integer;
	Begin
		If fileinput <> '' Then
			if (readGeneration(fic, grille)) Then
				exit();
		If israndom Then Begin
			WriteLn('Remplir avec cd de pourcent (herbe) :');
			ReadLn(i);
			WriteLn('Remplir avec cb de pourcent (Mouton) :');
			ReadLn(j);
			grille := MoutonProg.InitialiserGenerationRandom(i, j);
		End Else 
			grille := MoutonProg.InitialiserGeneration(position, position2);
	End;

Procedure RunMouton(israndom : Boolean; position, position2 : tabPosition; nombreGeneration : Integer; fileoutput : String;Var fic2 : Text; fileinput : String; Var fic : Text);
	Var grille : TypeGeneration;
		e, i : Integer;
		stop, first : Boolean;
	Begin	
		stop := False;
		MoutonInit(grille, israndom, position, position2, fileinput, fic);
		first := fileinput = '';
		fileinput := '';
		Repeat
			If first Then
				grille := MoutonProg.RunGeneration(grille, nombreGeneration)
			Else Begin first := True;
				MoutonProg.afficherGeneration(grille);
			End;
			WriteLn('Que voullez vous faire :');
			WriteLn('1 : Relancer la simulation');
			If nombreGeneration < 0 Then
				WriteLn('2 : Poursuivre');
			WriteLn('0 : Quitter');	
			e := 0;
			Repeat
				ReadLn(i);
				If i = 1 Then Begin
					MoutonInit(grille, israndom, position, position2, fileinput, fic);
					e := 1;
				End Else If (i = 2) And (nombreGeneration < 0) Then Begin
					e := 1;
				End Else Begin
					e := 1;
					stop := True;
				End;
			Until e <> 0;
		Until stop <> False;
		If fileoutput <> '' Then Begin
			WriteLn(fic2, '#Last Simulation : ');
			MoutonProg.WriteGeneration(grille, fic2);
		End;
	End;

Procedure LoupInit(Var grille : TypeGeneration; israndom : Boolean; position, position2, position3 : tabPosition; fileinput : String; Var fic : Text);
	Var i,j, k  : Integer;
	Begin
		If fileinput <> '' Then
			if (readGeneration(fic, grille)) Then
				exit();
		If israndom Then Begin
			WriteLn('Remplir avec cd de pourcent (herbe) :');
			ReadLn(i);
			WriteLn('Remplir avec cb de pourcent (Mouton) :');
			ReadLn(j);
			WriteLn('Remplir avec cb de pourcent (Loup) :');
			ReadLn(k);
			grille := LoupProg.InitialiserGenerationRandom(i, j, k);
		End Else 
			grille := LoupProg.InitialiserGeneration(position, position2, position3);
	End;

Procedure RunLoup(israndom : Boolean; position, position2, position3 : tabPosition; nombreGeneration : Integer; fileoutput : String;Var fic2 : Text; fileinput : String; Var fic : Text);
	Var grille : TypeGeneration;
		e, i : Integer;
		stop, first : Boolean;
	Begin	
		stop := False;
		LoupInit(grille, israndom, position, position2, position3, fileinput, fic);
		first := fileinput = '';
		fileinput := '';
		Repeat
			If first Then
				grille := LoupProg.RunGeneration(grille, nombreGeneration)
			Else Begin first := True;
				LoupProg.afficherGeneration(grille);
			End;
			WriteLn('Que voullez vous faire :');
			WriteLn('1 : Relancer la simulation');
			If nombreGeneration < 0 Then
				WriteLn('2 : Poursuivre');
			WriteLn('0 : Quitter');	
			e := 0;
			Repeat
				ReadLn(i);
				If i = 1 Then Begin
					LoupInit(grille, israndom, position, position2, position3, fileinput, fic);
					e := 1;
				End Else If (i = 2) And (nombreGeneration < 0) Then Begin
					e := 1;
				End Else Begin
					e := 1;
					stop := True;
				End;
			Until e <> 0;
		Until stop <> False;
		If fileoutput <> '' Then Begin
			WriteLn(fic2, '#Last Simulation : ');
			LoupProg.WriteGeneration(grille, fic2);
		End;
	End;


Procedure WriteCustom(Var fic2 : Text; gametype : String; position, positionH, positionM, positionL : tabPosition);
	Var i : Integer;
	Begin
		If gametype = 'Vie' Then Begin
			Write(fic2, 'Position=[');
			For i := 1 To M Do If (position[i].x <> -1) And (position[i].y <> -1) Then
				Write(fic2, '(', position[i].x, ' ', position[i].y, ')');
			WriteLn(fic2, ']');
		End Else If gametype = 'Herbe' Then Begin
			Write(fic2, 'PositionH=[');
			For i := 1 To M Do If (positionH[i].x <> -1) And (positionH[i].y <> -1) Then
				Write(fic2, '(', positionH[i].x, ' ', positionH[i].y, ')');
			WriteLn(fic2, ']');
		End Else If gametype = 'Mouton' Then Begin
			Write(fic2, 'PositionH=[');
			For i := 1 To M Do If (positionH[i].x <> -1) And (positionH[i].y <> -1) Then
				Write(fic2, '(', positionH[i].x, ' ', positionH[i].y, ')');
			WriteLn(fic2, ']');
			Write(fic2, 'PositionM=[');
			For i := 1 To M Do If (positionM[i].x <> -1) And (positionM[i].y <> -1) Then
				Write(fic2, '(', positionM[i].x, ' ', positionM[i].y, ')');
			WriteLn(fic2, ']');
		End Else If gametype = 'Loup' Then Begin
			Write(fic2, 'PositionH=[');
			For i := 1 To M Do If (positionH[i].x <> -1) And (positionH[i].y <> -1) Then
				Write(fic2, '(', positionH[i].x, ' ', positionH[i].y, ')');
			WriteLn(fic2, ']');
			Write(fic2, 'PositionM=[');
			For i := 1 To M Do If (positionM[i].x <> -1) And (positionM[i].y <> -1) Then
				Write(fic2, '(', positionM[i].x, ' ', positionM[i].y, ')');
			WriteLn(fic2, ']');
			Write(fic2, 'PositionL=[');
			For i := 1 To M Do If (positionL[i].x <> -1) And (positionL[i].y <> -1) Then
				Write(fic2, '(', positionL[i].x, ' ', positionL[i].y, ')');
			WriteLn(fic2, ']');
		End;
	End;

Procedure selectModif(Var grille : TypeGrille; Var grille2 : TypeGeneration; gametype : String; x, y : Integer);
	Begin
		If gametype = 'Vie' Then
			If grille[x][y] = 1 Then
				grille[x][y] := 0
			Else
				grille[x][y] := 1
		Else If gametype = 'Herbe' Then
			If grille2[x][y].herbep.alive Then
				grille2[x][y].herbep.alive := False
			Else
				grille2[x][y].herbep.alive := True
		Else If gametype = 'Mouton' Then Begin
			If (grille2[x][y].herbep.alive = False) And (grille2[x][y].Moutonp.alive = False) Then
				grille2[x][y].herbep.alive := True
			Else If (grille2[x][y].herbep.alive) And (grille2[x][y].Moutonp.alive = False) Then Begin
				grille2[x][y].herbep.alive := False;
				grille2[x][y].Moutonp.alive := True;
			End Else If (grille2[x][y].herbep.alive = False) And (grille2[x][y].Moutonp.alive) Then
				grille2[x][y].herbep.alive := True
			Else If (grille2[x][y].herbep.alive) And (grille2[x][y].Moutonp.alive) Then Begin
				grille2[x][y].herbep.alive := False;
				grille2[x][y].Moutonp.alive := False;
			End;
		End Else If gametype = 'Loup' Then Begin
			If (grille2[x][y].Moutonp.alive = False) And (grille2[x][y].herbep.alive = False) And (grille2[x][y].loupp.alive = False) Then
				grille2[x][y].herbep.alive := True
			Else If (grille2[x][y].loupp.alive = False) And (grille2[x][y].herbep.alive) And (grille2[x][y].Moutonp.alive = False) Then Begin
				grille2[x][y].herbep.alive := False;
				grille2[x][y].Moutonp.alive := True;
			End Else If (grille2[x][y].loupp.alive = False) And (grille2[x][y].herbep.alive = False) And (grille2[x][y].Moutonp.alive) Then
				grille2[x][y].herbep.alive := True
			Else If (grille2[x][y].loupp.alive = False) And (grille2[x][y].herbep.alive) And (grille2[x][y].Moutonp.alive) Then Begin
				grille2[x][y].herbep.alive := False;
				grille2[x][y].Moutonp.alive := False;
				grille2[x][y].loupp.alive := True;
			End Else If (grille2[x][y].loupp.alive) And (grille2[x][y].herbep.alive = False) And (grille2[x][y].Moutonp.alive = False) Then Begin
				grille2[x][y].herbep.alive := True;
			End Else If (grille2[x][y].loupp.alive) And (grille2[x][y].herbep.alive) And (grille2[x][y].Moutonp.alive = False) Then Begin
				grille2[x][y].herbep.alive := False;
				grille2[x][y].Moutonp.alive := True;
			End Else If (grille2[x][y].loupp.alive) And (grille2[x][y].herbep.alive = False) And (grille2[x][y].Moutonp.alive = True) Then Begin
				grille2[x][y].herbep.alive := True;
			End Else If (grille2[x][y].loupp.alive) And (grille2[x][y].herbep.alive) And (grille2[x][y].Moutonp.alive) Then Begin
				grille2[x][y].herbep.alive := False;
				grille2[x][y].Moutonp.alive := False;
				grille2[x][y].loupp.alive := False;
			End;
		End;
	End;

Procedure printSelect(grille : TypeGrille; grille2 : TypeGeneration; gametype : String; x, y : Integer);
	Var i, j: Integer;
	Begin
		ClrScr;
		For i := 0 To N-1 Do Begin
			For j := 0 To N-1 Do Begin
				If (j = y) And (i = x) Then
					TextBackGround(Magenta);
				If gametype = 'Vie' Then
					If grille[i][j] = 0 Then
						Write('  ')
					Else
						Write('O ')
				Else If gametype = 'Herbe' Then
					If grille2[i][j].herbep.alive = False Then 
						Write ('  ')
					Else Write ('# ')
				Else If gametype = 'Mouton' Then Begin
					If grille2[i][j].herbep.alive = False Then 
						Write ('-')
					Else
						Write ('h');
					If grille2[i][j].Moutonp.alive = False Then 
						Write ('- ')
					Else
						Write ('m ');
				End Else If gametype = 'Loup' Then Begin
					If grille2[i][j].herbep.alive = False Then 
						Write ('-')
					Else
						Write ('h');
					If grille2[i][j].Moutonp.alive = False Then 
						Write ('-')
					Else
						Write ('m');
					If grille2[i][j].loupp.alive = False Then 
						Write ('- ')
					Else
						Write ('l ');
				End;
				If (j = y) And (i = x) Then
					TextBackGround(Black);
			End;
			WriteLn('|');
		End;
	End;

Procedure returnPos(grille : TypeGrille; grille2 : TypeGeneration; gametype : String; Var position, positionH, positionM, positionL : tabPosition);
	Var x, i, j : Integer;
		pos : TypePosition;
	Begin
		x := 1;
		If gametype = 'Vie' Then
			For i := 0 To N-1 Do
				For j := 0 To N-1 Do
					If grille[i][j] = 1 Then Begin
						pos.x := i;
						pos.y := j;
						position[x] := pos;
						x := x + 1;
			 		End;
		x := 1;
		If gametype <> 'Vie' Then
			For i := 0 To N-1 Do
				For j := 0 To N-1 Do
					If grille2[i][j].herbep.alive Then Begin
						pos.x := i;
						pos.y := j;
						positionH[x] := pos;
						x := x + 1;
			 		End;
		x := 1;
		If (gametype <> 'Vie') And (gametype <> 'Herbe') Then
			For i := 0 To N-1 Do
				For j := 0 To N-1 Do
					If grille2[i][j].Moutonp.alive Then Begin
						pos.x := i;
						pos.y := j;
						positionM[x] := pos;
						x := x + 1;
			 		End;
		x := 1;
		If gametype = 'Loup' Then
			For i := 0 To N-1 Do
				For j := 0 To N-1 Do
					If grille2[i][j].loupp.alive Then Begin
						pos.x := i;
						pos.y := j;
						positionL[x] := pos;
						x := x + 1;
			 		End;
	End;

Procedure select(gametype : String; Var position, positionH, positionM, positionL : tabPosition);
	Var i, j, x, y : Integer;
		grille : TypeGrille;
		grille2 : TypeGeneration;
		pos : TypePosition;
		ch : char;
	Begin
		x := 2;
		y := 2;
		If (gametype = 'Vie') Then
			For i := 0 To N-1 Do
				For j := 0 To N-1 Do
					grille[i][j] := 0
		Else
			initialiserTableau(grille2);
		Repeat
			printSelect(grille, grille2, gametype, x, y);
			For j := 0 To N-1 Do Begin
				Write('--');
				If (gametype = 'Mouton') Or (gametype = 'Loup') Then
					Write('-');
				If gametype = 'Loup' Then
					Write('-');
			End;
			WriteLn('+');
			ch:=ReadKey;
			Case ch Of
				#75 : If y > 0 Then
					y := y - 1;
				#77 : If y < N-1 Then
					y := y + 1;
				#80 : If x < N-1 Then
					x := x + 1;
				#72 : If x > 0 Then
					x := x - 1;
				#32 : Begin 
					selectModif(grille, grille2, gametype, x, y);
				End;
			End;
			Until (ch=#13) Or (ch=#27);
			pos.x := -1;
			pos.y := -1;
			For i := 1 To M Do Begin
				position[i] := pos;
				positionH[i] := pos;
				positionM[i] := pos;
				positionL[i] := pos;
			End;
			returnPos(grille, grille2, gametype, position, positionH, positionM, positionL);
		
	End;


Procedure Run(gameType : String; position, positionH, positionM, positionL : tabPosition; isposition, israndom, ispositionH, ispositionM, ispositionL : Boolean; nombreGeneration : Integer; fileoutput, fileinput : String; Var fic : Text);
	Var
	fic2 : Text;
	Begin
		If fileoutput <> '' Then Begin
			Assign(fic2, fileoutput);
			ReWrite(fic2);
			WriteLn(fic2, gametype);
			If israndom Then
				WriteLn(fic2, 'Random')
			Else 
				WriteCustom(fic2, gametype, position, positionH, positionM, positionL);
			WriteLn(fic2, 'NombreGeneration=',nombreGeneration);
			WriteLn(fic2, '');
		End;
		ClrScr;
		If (gametype = 'Vie') And (isposition Or israndom) Then Begin
			WriteLn('Démarrage de la Simulation du JeuDeLaVie');
			Delay(1000);
			RunGameOfLife(israndom, position, nombreGeneration, fileoutput, fic2, fileinput, fic);
		End Else If (gametype = 'Herbe') And (ispositionH Or israndom) Then Begin
			WriteLn('Démarrage de la Simulation de l herbe');
			Delay(1000);
			runHerbe(israndom, positionH, nombreGeneration, fileoutput, fic2, fileinput, fic);
		End Else If (gametype = 'Mouton') And ((ispositionH And ispositionM) Or israndom) Then Begin
			WriteLn('Démarrage de la Simulation des Moutons');
			Delay(1000);
			runMouton(israndom, positionH, positionM, nombreGeneration, fileoutput, fic2, fileinput, fic);
		End Else If (gametype = 'Loup') And ((ispositionH And ispositionM And ispositionL) Or israndom) Then Begin
			WriteLn('Démarrage de la Simulation des loups');
			Delay(1000);
			runLoup(israndom, positionH, positionM, positionL, nombreGeneration, fileoutput, fic2, fileinput, fic);
		End Else Begin
			WriteLn('Erreur dans les paramètres du fichier !');	
		End;
		If fileoutput <> '' Then Close(fic2);
	End;

Procedure readFile(Var fic : Text; Var gametype : String; Var position, positionH, positionM, positionL : tabPosition; Var israndom, isposition, ispositionH, ispositionM, ispositionL : Boolean; Var nombreGeneration : Integer);
	Var 	ligne2, ligne3, ligne : String;
		nospace, egal, stop : Boolean;
		l, i, e : Integer;
	Begin
		Repeat
			ReadLn(fic,ligne);
			l := length(ligne);
			ligne2 := '';
			ligne3 := '';
			nospace := False;
			egal := False;
			stop := False;
			For i := 1 To l Do
				If ((ligne[i] <> ' ') And (ligne[i] <> '') Or nospace) And (stop = False) Then Begin
					If ligne[i] = '#' Then
						stop := True;
					If ligne[i] = '(' Then
						nospace := True
					Else If ligne[i] = ')' Then
						nospace := False;
					If (ligne[i] = '=') Then
						egal := True;
					If egal And (ligne[i] <> '=') Then
						ligne3 := ligne3 + ligne[i]
					Else If egal = False Then
						ligne2 := ligne2 + ligne[i];
				End;
			If ligne2 <> '' Then Begin
				If (ligne2 = 'Vie') Or (ligne2 = 'Herbe') Or (ligne2 = 'Mouton') Or (ligne2 = 'Loup') Then
					gametype := ligne2
				Else If ligne2 = 'NombreGeneration' Then Begin
					Val(ligne3, nombreGeneration, e);
					If e <> 0 Then Begin
						WriteLn('Nombre de génération incorrecte !');		
						halt;
					End;
				End Else If ligne2 = 'Position' Then Begin
					position := getList(ligne3);
					isposition := True;
				End Else If ligne2 = 'PositionH' Then Begin
					positionH := getList(ligne3);
					ispositionH := True;
				End Else If ligne2 = 'PositionM' Then Begin
					positionM := getList(ligne3);
					ispositionM := True;
				End Else If ligne2 = 'PositionL' Then Begin
					positionL := getList(ligne3);
					ispositionL := True;
				End Else If ligne2 = 'Random' Then
					israndom := True;
			End;
		Until eof(fic);
	End;

Procedure manualMenu(Var gametype : String; Var israndom, isposition, ispositionH, ispositionM, ispositionL : Boolean; Var position, positionH, positionM, positionL : tabPosition; Var nombreGeneration : Integer);
	Var e, i : Integer;
	Begin
		WriteLn('Vous n avez pas précisé le fichier input, vous pouvez Tout de même lancer une simulation.');
		WriteLn('Choississez le mode de jeu :');
		WriteLn('1 : Vie');
		WriteLn('2 : Herbe');
		WriteLn('3 : Mouton');
		WriteLn('4 : Loup');
		WriteLn('0 : Quit');
		Repeat
			ReadLn(i);
			If i = 1 Then
				gametype := 'Vie'
			Else If i = 2 Then
				gametype := 'Herbe'
			Else If i = 3 Then
				gametype := 'Mouton'
			Else If i = 4 Then
				gametype := 'Loup'
			Else If i = 0 Then
				halt;
		Until gametype <> '';
		WriteLn('Type d entré');
		WriteLn('1 : Random');
		WriteLn('2 : Manuelle');
		WriteLn('0 : Quit');
		e := 0;
		Repeat
			ReadLn(i);
			If i = 1 Then Begin
				e := 1;
				israndom := True;
			End Else If i = 2 Then Begin
				e := 1;
				israndom := False;
				isposition := True;
				ispositionH := True;
				ispositionM := True;
				ispositionL := True;
			End Else If i = 0 Then
				halt;
		Until e <> 0;
		If israndom = False Then Begin
			select(gametype, position, positionH, positionM, positionL);
		End;
		WriteLn('Nombre de génération :');
		ReadLn(nombreGeneration);
	End;

Var
	gametype, fileoutput, fileinput : String;
	fic : Text;
	position, positionH, positionM, positionL : tabPosition;
	isposition, ispositionH, ispositionM, ispositionL, israndom : Boolean;
	nombreGeneration : Integer;
Begin	
	Randomize;
	If ParamCount >= 2 Then
		getFile(1, fileoutput, fileinput);
	If ParamCount = 4 Then
		getFile(3, fileoutput, fileinput);
	isposition := False;
	ispositionH := False;	
	ispositionM := False;
	ispositionL := False;
	israndom := False;
	{$i-}
	If fileinput <> '' Then Begin
		assign(fic, fileinput);
		reset(fic);
		If(IOresult <> 0) Then Begin
			WriteLn('lefichier '+ fileinput +' n existe pas');
			halt;
		End Else Begin 
			readFile(fic, gametype, position, positionH, positionM, positionL, israndom, isposition, ispositionH, ispositionM, ispositionL, nombreGeneration);
			If isposition And (ispositionH = False) Then Begin
				positionH := position;
				ispositionH := True;
			End;
			Run(gametype, position, positionH, positionM, positionL, isposition, israndom, ispositionH, ispositionM, ispositionL, nombreGeneration, fileoutput, fileinput, fic);
			close(fic);
		End;
	End Else Begin
		manualMenu(gametype, israndom, isposition, ispositionH, ispositionM, ispositionL, position, positionH, positionM, positionL, nombreGeneration);
		Run(gametype, position, positionH, positionM, positionL, isposition, israndom, ispositionH, ispositionM, ispositionL, nombreGeneration, fileoutput, fileinput, fic);
	End;
	WriteLn('Type de simulation : ',gametype);
	If israndom Then
		WriteLn('Type de remplissage : Random')
	Else
		WriteLn('Type de remplissage : Manuelle');
	WriteLn('Nombre d intéractions : ',nombreGeneration);
End.
