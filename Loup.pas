PROGRAM LoupProg;
Uses Utils, Crt;

Function InitialiserGeneration(tableau, tableau2, tableau3 : TabPosition) : TypeGeneration;
	Var i, x, y : Integer;
		grille : TypeGeneration;
	Begin
		initialiserTableau(grille);
		For i := 1 To M Do Begin
			x := tableau[i].x;
			y := tableau[i].y;
			If (x <> -1) And (y <> -1) Then Begin
				grille[x][y].moutonp.energie := MOUTON_ENERGIE_INITIALE;
				grille[x][y].moutonp.alive := True;
			End;
		End;
		For i := 1 To M Do Begin
			x := tableau2[i].x;
			y := tableau2[i].y;
			If (x <> -1) And (y <> -1) Then Begin
				grille[x][y].herbep.energie := HERBE_ENERGIE_INITIALE;
				grille[x][y].herbep.alive := True;
			End;
		End;
		For i := 1 To M Do Begin
			x := tableau3[i].x;
			y := tableau3[i].y;
			If (x <> -1) And (y <> -1) Then Begin
				grille[x][y].loupp.energie := 4;
				grille[x][y].loupp.alive := True;
			End;
		End;
		InitialiserGeneration := grille;
	End;

function compagnon(grille : TypeGeneration; x, y : integer):boolean;
var
begin
		For i := x-1 To x+1 Do Begin
			For j := y-1 To y+1 Do Begin
end;

Function caseLibreLoup(x, y : Integer; loup2 : Loup; grille, grille2 : TypeGeneration) : TypePosition;
	Var i,j, x2, y2, c : Integer;
		tabPosition2 : tabPosition;
		typePosition2 : TypePosition;
	Begin
		c := 0;
		For i := x-1 To x+1 Do Begin
			If i = -1 Then x2 := N-1 Else If i = N Then x2 := 0 Else x2 := i;
			If (grille[x2][y].loupp.alive = False) and (grille2[x2][y].loupp.alive = False) Then Begin
				typePosition2.x := x2;
				typePosition2.y := y;
				tabPosition2[c] := typePosition2;
				c := c + 1;
			End;
		End;
		For j := y-1 To y+1 Do Begin
			If j = -1 Then y2 := N-1 Else If j = N Then y2 := 0 Else y2 := j;
			If (grille[x][y2].loupp.alive = False) and (grille2[x][y2].loupp.alive = False) Then Begin
				typePosition2.x := x;
				typePosition2.y := y2;
				tabPosition2[c] := typePosition2;
				c := c + 1;
			End;			
		End;
		If c = 0 Then Begin
			typePosition2.x := -1;
			typePosition2.y := -1;
			exit(typePosition2) ;
		End;
		c := random(c);
		exit(tabPosition2[c]);
	End;

Procedure AfficherGeneration(grille : TypeGeneration);
	Var i,j : Integer;	
	Begin
		For i := 0 To N-1 Do Begin
			For j := 0 To N-1 Do Begin
				If grille[i][j].herbep.alive = False Then 
					Write ('-')
				Else
					Write ('h');
				If grille[i][j].moutonp.alive = False Then 
					Write ('- ')
				Else
					Write ('m ');	
				If grille[i][j].loupp.alive = False Then 
					Write ('-')
				Else
					Write ('l');				
			End;
			WriteLn;
		End;
	End;

Function Run(grille : TypeGeneration) : TypeGeneration;
	Var i, j : integer;
		varHerbe, herbe2: Herbe;
		varMouton, mouton2: Mouton;
		varLoup, loup2: Loup;
		grille2 : typeGeneration;
		typePosition2 : TypePosition;
	Begin
		For i := 0 To N-1 Do Begin
			For j := 0 To N-1 Do Begin
				varHerbe.energie := 0;
				varHerbe.age := 0;
				varHerbe.alive := False;
				grille2[i][j].herbep := varHerbe;
				varMouton.energie := 0;
				varMouton.age := 0;
				varMouton.alive := False;
				grille2[i][j].moutonp := varMouton;
			End;
		End;
		For i := 0 To N-1 Do Begin
			For j := 0 To N-1 Do Begin
				herbe2 := grille[i][j].herbep;
				mouton2 := grille[i][j].moutonp;
				If herbe2.alive = True Then Begin
					If herbe2.age < HERBE_AGE_MAX Then Begin
						herbe2.age := herbe2.age+1;
						herbe2.energie :=herbe2.energie+HERBE_ENERGIE; 
						If herbe2.energie >= HERBE_ENERGIE_REPRODUCTION Then Begin
							reproductionherbe(i,j, herbe2,grille, grille2);
							herbe2.energie := herbe2.energie-HERBE_ENERGIE_REPRODUCTION;
						End;
					End
				Else Begin
					herbe2.age:=0;
					herbe2.energie:=0;
					herbe2.alive:= False;
				End;
				grille2[i][j].herbep:=herbe2;
			End;	
		If mouton2.alive = True Then Begin
				If (mouton2.age < MOUTON_AGE_MAX) And (mouton2.energie > 0) Then Begin
					mouton2.age := mouton2.age + 1;
					If herbe2.alive = True Then Begin
						herbe2.age:=0;
						herbe2.energie:=0;
						herbe2.alive:= False;
						mouton2.energie := mouton2.energie+MOUTON_ENERGIE;
						grille2[i][j].herbep:=herbe2;
					End
					Else If mouton2.energie >= MOUTON_ENERGIE_REPRODUCTION Then Begin
						reproductionmouton(i,j,mouton2,grille,grille2);
						mouton2.energie := mouton2.energie-MOUTON_ENERGIE_REPRODUCTION ;
					End
					Else Begin
						typePosition2 := priomouton(i,j,mouton2,grille, grille2);
						If (typePosition2.x = -1) Or (typePosition2.y = -1) Then
							typePosition2 := caselibremouton(i,j,mouton2,grille, grille2);
							If (typePosition2.x <> -1) And (typePosition2.y <> -1) Then Begin
								mouton2.energie := mouton2.energie-2;
								grille2[typePosition2.x][typePosition2.y].moutonp:=mouton2;
								mouton2.age:=0;
								mouton2.energie:=0;
								mouton2.alive:= False;
							End
							Else
								mouton2.energie := mouton2.energie-1;
						End
					End
					Else Begin
						mouton2.age:=0;
						mouton2.energie:=0;
						mouton2.alive:= False;
					End;
					grille2[i][j].moutonp:=mouton2;
				End;	
		If loup2.alive = True Then Begin
				If (loup2.age < LOUP_AGE_MAX) And (loup2.energie > 0) Then Begin
					loup2.age := loup2.age + 1;
					If mouton2.alive = True Then Begin
						mouton2.age:=0;
						mouton2.energie:=0;
						mouton2.alive:= False;
						loup2.energie := loup2.energie+LOUP_ENERGIE;
						grille2[i][j].moutonp:=mouton2;
					End
					Else If ((loup2.energie >= LOUP_ENERGIE_REPRODUCTION) and (compagnon(grille,i,j) = true ) and (loup2.age >= 2)) Then Begin
						reproductionloup(i,j,loup2,grille,grille2);
						loup2.energie := loup2.energie-LOUP_ENERGIE_REPRODUCTION ;
					End
					Else Begin
						typePosition2 := prioloup(i,j,loup2,grille, grille2);
						If (typePosition2.x = -1) Or (typePosition2.y = -1) Then
							typePosition2 := caselibreloup(i,j,mouton2,grille, grille2);
							If (typePosition2.x <> -1) And (typePosition2.y <> -1) Then Begin
								loup2.energie := loup2.energie-2;
								grille2[typePosition2.x][typePosition2.y].loupp:=loup2;
								loup2.age:=0;
								loup2.energie:=0;
								loup2.alive:= False;
							End
							Else
								loup2.energie := loup2.energie-2;
						End
					End
					Else Begin
						loup2.age:=0;
						loup2.energie:=0;
						loup2.alive:= False;
					End;
					grille2[i][j].loupp:=loup2;
				End;				 
			End;
		End;
		Run := grille2;
	End;

Function RunGeneration(grille : TypeGeneration; nombreIteration : Integer) : TypeGeneration;
	Var i : integer;
	Begin
		If nombreIteration > 0 Then
		For i := 1 To nombreIteration Do Begin
			grille := Run(grille);
			ClrScr;
		End
		Else Begin
			Repeat
				grille := Run(grille);
				Delay(100);
				ClrScr;
				AfficherGeneration(grille);
			Until KeyPressed;
			Halt;
		End;
		RunGeneration := grille
	End;
	
Var varTypePosition : TypePosition;
	grille : TypeGeneration;
	listpos, listpos2, listpos3 : TabPosition;
	nbcn, i : Integer;
Begin
	Randomize;
	For i:=1 To M Do Begin
		varTypePosition.x := -1;
		varTypePosition.y := -1;
		listpos[i] := varTypePosition;
		listpos2[i] := varTypePosition;
	End;
	WriteLn('Combien de case à remplir (herbe) ?');
	initManuel(listpos);
	WriteLn('Combien de case à remplir (mouton) ?');
	initManuel(listpos2);
	WriteLn('Combien de case à remplir (loup) ?');
	initManuel(listpos3);
	grille := InitialiserGeneration(listpos, listpos2, listpos3);
	WriteLn('Combien d interactions ?');
	Readln(nbcn);
	AfficherGeneration(RunGeneration(grille, nbcn));
End.
