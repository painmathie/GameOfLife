Unit LoupProg;
Interface
Uses Utils, Crt;
Function InitialiserGeneration(tableau, tableau2, tableau3 : TabPosition) : TypeGeneration;
function compagnon(grille : TypeGeneration; x, y : integer):boolean;
Function caseLibreLoup(x, y : Integer; loup2 : Loup; grille, grille2 : TypeGeneration) : TypePosition;
Function prioLoup(x, y : Integer; loup2 : Loup; grille, grille2 : TypeGeneration) : TypePosition;
Procedure reproductionLoup(x, y : Integer; loup2 : Loup; grille : TypeGeneration; Var grille2 : TypeGeneration);
Procedure AfficherGeneration(grille : TypeGeneration);
Procedure WriteGeneration(grille : TypeGeneration; Var  fic2 : Text);
Function Run(grille : TypeGeneration) : TypeGeneration;
Function RunGeneration(grille : TypeGeneration; nombreIteration : Integer) : TypeGeneration;
Function InitialiserGenerationRandom(nb, nb2, nb3 : Integer) : TypeGeneration;
Procedure CompteMoutonHerbeLoup(grille : TypeGeneration; Var x, y, z : Integer);
Implementation


Function InitialiserGenerationRandom(nb, nb2, nb3 : Integer) : TypeGeneration;
	Var i, j, r, r2, r3: Integer;
		grille : TypeGeneration;
		varHerbe : Herbe;
		varMouton : Mouton;
		varLoup : Loup;
	Begin
		initialiserTableau(grille);
		For i := 0 To N-1 Do
			For j := 0 To N-1 Do Begin
				r := Random(100);
				r2 := Random(100);
				r3 := Random(100);
				If r <= nb Then begin
					varHerbe.energie := HERBE_ENERGIE_INITIALE;
					varHerbe.age := 0;
					varHerbe.alive := True;
					grille[i][j].herbep := varHerbe;
				End;
				If r2 <= nb2 Then Begin
					varMouton.energie := MOUTON_ENERGIE_INITIALE;
					varMouton.age := 0;
					varMouton.alive := True;
					grille[i][j].moutonp := varMouton;
				End;
				If r3 <= nb3 Then Begin
					varLoup.energie := LOUP_ENERGIE_INITIALE;
					varLoup.age := 0;
					varLoup.alive := True;
					grille[i][j].loupp := varLoup;
				End;
			end;
		InitialiserGenerationRandom := grille;
	End;


Function InitialiserGeneration(tableau, tableau2, tableau3 : TabPosition) : TypeGeneration;
	Var i, x, y : Integer;
		grille : TypeGeneration;
	Begin
		initialiserTableau(grille);
		For i := 1 To M Do Begin
			x := tableau[i].x;
			y := tableau[i].y;
			If (x <> -1) And (y <> -1) Then Begin
				grille[x][y].moutonp.energie := MOUTON_ENERGIE_INITIALE;
				grille[x][y].moutonp.alive := True;
			End;
		End;
		For i := 1 To M Do Begin
			x := tableau2[i].x;
			y := tableau2[i].y;
			If (x <> -1) And (y <> -1) Then Begin
				grille[x][y].herbep.energie := HERBE_ENERGIE_INITIALE;
				grille[x][y].herbep.alive := True;
			End;
		End;
		For i := 1 To M Do Begin
			x := tableau3[i].x;
			y := tableau3[i].y;
			If (x <> -1) And (y <> -1) Then Begin
				grille[x][y].loupp.energie := 4;
				grille[x][y].loupp.alive := True;
			End;
		End;
		InitialiserGeneration := grille;
	End;

function compagnon(grille : TypeGeneration; x, y : integer):boolean;
var i,j,x2,y2 : integer ;
begin
		For i := x-1 To x+1 Do
			If i = -1 Then x2 := N-1 Else If i = N Then x2 := 0 Else x2 := i;
				if (x <> x2) and (grille[x2][y].loupp.alive = True) Then
						exit (true);
		For j := y-1 To y+1 Do
			If j = -1 Then y2 := N-1 Else If j = N Then y2 := 0 Else y2 := j;
				if (y <> y2) and (grille[x][y2].loupp.alive = true) Then
						exit(true);
	compagnon := false;
end;

Procedure WriteGeneration(grille : TypeGeneration; var  fic2 : Text);
	Var i,j : Integer;	
	Begin
		For i := 0 To N-1 Do Begin
			write(fic2, '#');
			For j := 0 To N-1 Do Begin
				If grille[i][j].herbep.alive = False Then 
					Write (fic2, '-')
				Else
					Write (fic2, 'h');
				If grille[i][j].moutonp.alive = False Then 
					Write (fic2, '- ')
				Else
					Write (fic2, 'm ');
				If grille[i][j].loupp.alive = False Then 
					Write (fic2, '- ')
				Else
					Write (fic2, 'l ');	
			End;
			Writeln (fic2, '');
		End;
		
		saveGrille(fic2, grille);
	End;

Function caseLibreLoup(x, y : Integer; loup2 : Loup; grille, grille2 : TypeGeneration) : TypePosition;
	Var i,j, x2, y2, c : Integer;
		tabPosition2 : tabPosition;
		typePosition2 : TypePosition;
	Begin
		c := 0;
		For i := x-1 To x+1 Do Begin
			If i = -1 Then x2 := N-1 Else If i = N Then x2 := 0 Else x2 := i;
			If (grille[x2][y].loupp.alive = False) and (grille2[x2][y].loupp.alive = False) Then Begin
				typePosition2.x := x2;
				typePosition2.y := y;
				tabPosition2[c] := typePosition2;
				c := c + 1;
			End;
		End;
		For j := y-1 To y+1 Do Begin
			If j = -1 Then y2 := N-1 Else If j = N Then y2 := 0 Else y2 := j;
			If (grille[x][y2].loupp.alive = False) and (grille2[x][y2].loupp.alive = False) Then Begin
				typePosition2.x := x;
				typePosition2.y := y2;
				tabPosition2[c] := typePosition2;
				c := c + 1;
			End;			
		End;
		If c = 0 Then Begin
			typePosition2.x := -1;
			typePosition2.y := -1;
			exit(typePosition2) ;
		End;
		c := random(c);
		exit(tabPosition2[c]);
	End;

Function prioLoup(x, y : Integer; loup2 : Loup; grille, grille2 : TypeGeneration) : TypePosition;
	Var i,j, x2, y2, c : Integer;
	a : Real;
		tabPosition2, tabPosition3 : TabPosition;
		typePosition2 : TypePosition;
	Begin
		c := 0;
		If(loup2.energie > LOUP_ENERGIE_REPRODUCTION+6) then Begin
			For i := x-5 To x+5 Do Begin
				If i < 0 Then x2 := N+i Else If i > N Then x2 := i-N Else x2 := i;
				For j := y-5 To y+5 Do Begin
					If j < 0 Then y2 := N+i Else If j > N Then y2 := j-N Else y2 := j;
					If (x <> x2) and (y <> y2) and (grille[x2][y2].loupp.alive = True) and (grille[x2][y2].loupp.age = LOUP_AGE_MAX-1) Then Begin
						typePosition2.x := i;
						typePosition2.y := j;
						tabPosition3[c] := typePosition2;
						c := c + 1;
					End;			
				End;
			End;
			a := 444;
			If c <> 0 Then Begin
				for i := 0 to c do begin
					if a > sqrt(tabPosition3[i].x*tabPosition3[i].x + tabPosition3[i].y*tabPosition3[i].y) Then Begin
						a := sqrt(tabPosition3[i].x*tabPosition3[i].x + tabPosition3[i].y*tabPosition3[i].y);
						typePosition2.x := tabPosition3[i].x;
						typePosition2.y := tabPosition3[i].y;
					End;
				End;
				
				If typePosition2.x > x Then typePosition2.x := 1 Else If typePosition2.x < x Then typePosition2.x := -1 Else typePosition2.x := 0;
				If typePosition2.y > y Then typePosition2.y := 1 Else If typePosition2.y < y Then typePosition2.y := -1 Else typePosition2.y := 0;
				exit(typePosition2);
			End;

		End;
		For i := x-1 To x+1 Do Begin
			If i = -1 Then x2 := N-1 Else If i = N Then x2 := 0 Else x2 := i;
			If (grille[x2][y].loupp.alive = False) And (grille2[x2][y].loupp.alive = False) And (grille[x2][y].moutonp.alive = True) Then Begin
				typePosition2.x := x2;
				typePosition2.y := y;
				tabPosition2[c] := typePosition2;
				c := c + 1;
			End;
		End;
		For j := y-1 To y+1 Do Begin
			If j = -1 Then y2 := N-1 Else If j = N Then y2 := 0 Else y2 := j;
			If (grille[x][y2].loupp.alive = False) And (grille2[x][y2].loupp.alive = False) And (grille[x][y2].moutonp.alive = True) Then Begin
				typePosition2.x := x;
				typePosition2.y := y2;
				tabPosition2[c] := typePosition2;
				c := c + 1;
			End;			
		End;
		If c = 0 Then Begin
			typePosition2.x := -1;
			typePosition2.y := -1;
			exit(typePosition2) ;
		End;
		c := random(c);
		exit(tabPosition2[c]);
	End;

Procedure reproductionLoup(x, y : Integer; loup2 : Loup; grille : TypeGeneration; Var grille2 : TypeGeneration);
	Var typePosition2 : TypePosition;
	Begin
		typePosition2 := caseLibreLoup(x, y, loup2, grille, grille2);
		If (typePosition2.x <> -1) And (typePosition2.y <> -1) Then Begin
			grille2[typePosition2.x][typePosition2.y].loupp.alive := True;
			grille2[typePosition2.x][typePosition2.y].loupp.energie := LOUP_ENERGIE_INITIALE;
		End;
	End;


Procedure AfficherGeneration(grille : TypeGeneration);
	Var i,j : Integer;	
	Begin
		For i := 0 To N-1 Do Begin
			For j := 0 To N-1 Do Begin
				If grille[i][j].herbep.alive = True Then 
					TextBackground(Green);
				If grille[i][j].moutonp.alive = False Then 
					Write (' ')
				Else
					Write ('m');	
				TextColor(Red);
				If grille[i][j].loupp.alive = False Then 
					Write ('  ')
				Else
					Write ('L ');
				TextColor(White);
				TextBackground(Black);			
			End;
			WriteLn('|');
		End;
		For j := 0 To N-1 Do
			Write ('---');
		Writeln ('+');
	End;

Function Run(grille : TypeGeneration) : TypeGeneration;
	Var i, j : integer;
		herbe2: Herbe;
		mouton2: Mouton;
		loup2: Loup;
		grille2 : typeGeneration;
		typePosition2 : TypePosition;
	Begin
		initialiserTableau(grille2);
		For i := 0 To N-1 Do Begin
			For j := 0 To N-1 Do Begin
				herbe2 := grille[i][j].herbep;
				mouton2 := grille[i][j].moutonp;
				loup2 := grille[i][j].loupp;
				If herbe2.alive = True Then Begin
					If herbe2.age < HERBE_AGE_MAX Then Begin
						herbe2.age := herbe2.age+1;
						herbe2.energie :=herbe2.energie+HERBE_ENERGIE; 
						If herbe2.energie >= HERBE_ENERGIE_REPRODUCTION Then Begin
							reproductionherbe(i,j, herbe2,grille, grille2);
							herbe2.energie := herbe2.energie-HERBE_ENERGIE_REPRODUCTION;
						End;
					End
				Else Begin
					herbe2.age:=0;
					herbe2.energie:=0;
					herbe2.alive:= False;
				End;
				grille2[i][j].herbep:=herbe2;
			End;	
		If mouton2.alive = True Then Begin
				If (mouton2.age < MOUTON_AGE_MAX) And (mouton2.energie > 0) Then Begin
					mouton2.age := mouton2.age + 1;
					If herbe2.alive = True Then Begin
						herbe2.age:=0;
						herbe2.energie:=0;
						herbe2.alive:= False;
						mouton2.energie := mouton2.energie+MOUTON_ENERGIE;
						grille2[i][j].herbep:=herbe2;
					End
					Else If mouton2.energie >= MOUTON_ENERGIE_REPRODUCTION Then Begin
						reproductionmouton(i,j,mouton2,grille,grille2);
						mouton2.energie := mouton2.energie-MOUTON_ENERGIE_REPRODUCTION ;
					End
					Else Begin
						typePosition2 := priomouton(i,j,mouton2,grille, grille2);
						If (typePosition2.x = -1) Or (typePosition2.y = -1) Then
							typePosition2 := caselibremouton(i,j,mouton2,grille, grille2);
							If (typePosition2.x <> -1) And (typePosition2.y <> -1) Then Begin
								mouton2.energie := mouton2.energie-2;
								grille2[typePosition2.x][typePosition2.y].moutonp:=mouton2;
								mouton2.age:=0;
								mouton2.energie:=0;
								mouton2.alive:= False;
							End
							Else
								mouton2.energie := mouton2.energie-1;
						End
					End
					Else Begin
						mouton2.age:=0;
						mouton2.energie:=0;
						mouton2.alive:= False;
					End;
					grille2[i][j].moutonp:=mouton2;
				End;	
		If loup2.alive = True Then Begin
				If (loup2.age < LOUP_AGE_MAX) And (loup2.energie > 0) Then Begin
					loup2.age := loup2.age + 1;
					If mouton2.alive = True Then Begin
						mouton2.age:=0;
						mouton2.energie:=0;
						mouton2.alive:= False;
						loup2.energie := loup2.energie+LOUP_ENERGIE;
						grille2[i][j].moutonp:=mouton2;
					End
					Else If ((loup2.energie >= LOUP_ENERGIE_REPRODUCTION) and (compagnon(grille,i,j) = true ) and (loup2.age >= 2)) Then Begin
						reproductionloup(i,j,loup2,grille,grille2);
						loup2.energie := loup2.energie-LOUP_ENERGIE_REPRODUCTION ;
					End
					Else Begin
						typePosition2 := prioloup(i,j,loup2,grille, grille2);
						If (typePosition2.x = -1) Or (typePosition2.y = -1) Then
							typePosition2 := caselibreloup(i,j,loup2,grille, grille2);
							If (typePosition2.x <> -1) And (typePosition2.y <> -1) Then Begin
								loup2.energie := loup2.energie-2;
								grille2[typePosition2.x][typePosition2.y].loupp:=loup2;
								loup2.age:=0;
								loup2.energie:=0;
								loup2.alive:= False;
							End
							Else
								loup2.energie := loup2.energie-2;
						End
					End
					Else Begin
						loup2.age:=0;
						loup2.energie:=0;
						loup2.alive:= False;
					End;
					grille2[i][j].loupp:=loup2;
				End;				 
			End;
		End;
		Run := grille2;
	End;



Procedure CompteMoutonHerbeLoup(grille : TypeGeneration; Var x, y, z : Integer);
Var i,j: Integer;
Begin
	x := 0;
	y := 0;
	z := 0;
	For i := 0 To N-1 Do
		For j := 0 To N-1 Do Begin
			If grille[i][j].herbep.alive Then
				x := x + 1;
			If grille[i][j].moutonp.alive Then
				y := y + 1;
			If grille[i][j].loupp.alive Then
				z := z + 1;
			End;
End;

Function RunGeneration(grille : TypeGeneration; nombreIteration : Integer) : TypeGeneration;
	Var i, x, y, z : integer;
	Begin
		ClrScr;
		If nombreIteration > 0 Then begin
			AfficherGeneration(grille);
			Delay(1000);
			For i := 1 To nombreIteration Do Begin
				grille := Run(grille);
				ClrScr;
			End;
			AfficherGeneration(grille);
			CompteMoutonHerbeLoup(grille, x, y, z);
			WriteLn(x, ' herbes vivantes.');
			WriteLn(y, ' moutons vivants.');
			WriteLn(z, ' loups vivants.');
		end
		Else Begin
			Repeat
				grille := Run(grille);
				Delay(100);
				ClrScr;
				AfficherGeneration(grille);
				CompteMoutonHerbeLoup(grille, x, y, z);
				WriteLn(x, ' herbes vivantes.');
				WriteLn(y, ' moutons vivants.');
				WriteLn(z, ' loups vivants.');
			Until KeyPressed;
		End;
		RunGeneration := grille
	End;
End.
