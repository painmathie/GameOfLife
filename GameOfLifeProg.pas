
// Voici les fonctions qui nous permettent de faire fonctionner le programme de GameOfLife correctement.


unit GameOfLifeProg;
interface
Uses Crt, Utils;
Type TypeGrille = Array[0..N-1,0..N-1] of Integer;

Function RemplirGrille(tableau : TabPosition) : TypeGrille;
Function InitGrille(nb : Integer) : TypeGrille;
Function CompteCellule(grille : TypeGrille) : Integer;
Function CountAroundCase(grille : TypeGrille; x, y : Integer) : Integer;
Function CalculerValeurCellule(grille : TypeGrille; x, y : Integer) : Integer;
Function CalculerNouvelleGrille(grille : TypeGrille) : TypeGrille;
Procedure AfficherGrille(grille : TypeGrille);
Procedure WriteGrille(grille : TypeGrille; var fic2 : Text);
Function Run(grilleInitiale : TypeGrille; n : Integer) : TypeGrille;
implementation



Function RemplirGrille(tableau : TabPosition) : TypeGrille;
	Var i, j, x, y : Integer;
		grille : TypeGrille;
	Begin
		For i := 0 To N-1 Do
			For j := 0 To N-1 Do
				grille[i][j] := 0;
		For i := 1 To M Do Begin
			x := tableau[i].x;
			y := tableau[i].y;
			If (x <> -1) And (y <> -1) Then
				grille[x][y] := 1;
		End;
		RemplirGrille := grille;
	End;


//Cette fonction selon permet dans un premier temps d'initialiser la grille à zéro , mais elle permet également de définir manuellement la position dans le tableau des éléments au départ (à l'aide des -1 et des 1)
 


Function InitGrille(nb : Integer) : TypeGrille;
	Var i, j, r: Integer;
		grille : TypeGrille;
	Begin
		For i := 0 To N-1 Do
			For j := 0 To N-1 Do Begin
				r := Random(100);
				If r <= nb Then
					grille[i][j] := 1
				Else
					grille[i][j] := 0;
			End;
		InitGrille := grille;
	End;

// Celle ci, nous permets grace a un 'random', de pouvoir rentrer un integer, qui sera le pourcentage de case '1' dans la grille rempli par rapport au nombre de case totale. Autrement dis, il s'agit de l'initialisation de l'aléatoire de la grille.

 

Function CompteCellule(grille : TypeGrille) : Integer;
	Var i, j ,count : Integer;
	Begin
		count := 0;
		For i := 0 To N-1 Do
			For j := 0 To N-1 Do
				count := count + grille[i][j];
		CompteCellule := count;
	End;

// Cela sert a coompter les cellules ou il y a un 1.



Function CountAroundCase(grille : TypeGrille; x, y : Integer) : Integer;
	Var i, j, x2, y2, count : Integer;
	Begin
		count := -grille[x][y];
		For i := x-1 To x+1 Do Begin
			If i = -1 Then
				x2 := N-1
			Else If i = N Then
				x2 := 0
			Else
				x2 := i;
			For j := y-1 To y+1 Do Begin
				If j = -1 Then
					y2 := N-1
				Else If j = N Then
					y2 := 0
				Else
					y2 := j;
				count := count + grille[x2][y2];		
			End;
		End;
		CountAroundCase := count;
	End;

//cette fonction sert a compter le nombre de '1' voisin a la case.



Function CalculerValeurCellule(grille : TypeGrille; x, y : Integer) : Integer;
	Var count, r : Integer;
	Begin
		count := CountAroundCase(grille, x, y);
		If grille[x, y] = 1 Then
			If (count = 2) Or (count = 3) Then
				r := 1
			Else
				r := 0
		Else If count = 3 Then
			r := 1
		Else
			r := 0;
		CalculerValeurCellule := r;
	End;

//celle ci permet de calculer la nouvelle valeur d'une case en fonction de ses voisines.


Function CalculerNouvelleGrille(grille : TypeGrille) : TypeGrille;
	Var i, j : Integer ; 
		grille2 : TypeGrille;
	Begin
		For i := 0 To N-1 Do
			For j := 0 To N-1 Do
				grille2[i][j] := CalculerValeurCellule(grille, i, j);
		CalculerNouvelleGrille := grille2;
	End;

//Applique la fonction précédente à toute les cases de la grille


Procedure AfficherGrille(grille : TypeGrille);
	Var i, j : Integer;
	Begin
		For i := 0 To N-1 Do Begin
			For j := 0 To N-1 Do
				If grille[i][j] = 0 Then
					Write('  ')
				Else
					Write('O ');
				Writeln('|');
		End;
		For j := 0 To N-1 Do
			write('--');
		writeln('+');
	End;

//Cette fonction permet l'affichage de la grille


Procedure WriteGrille(grille : TypeGrille; var fic2 : Text);
	Var i, j, r : Integer;
	Begin
		For i := 0 To N-1 Do Begin
			write(fic2, '#');
			For j := 0 To N-1 Do
				If grille[i][j] = 0 Then
					Write(fic2, ' ')
				Else
					Write(fic2, 'O');
			Writeln(fic2, '');
		End;
		r := 0;
		WriteLn(fic2, '');
		Write(fic2, '$');
		For i := 0 To N-1 Do Begin
			For j := 0 To N-1 Do Begin
				Write (fic2, grille[i][j]);
				Write(fic2, ';');
				r := r + 1;
				If r >= 125 Then Begin
					r := 0;
					WriteLn(fic2, '');
				End;
			End;
		End;
		WriteLn(fic2, '$');
	End;


//permet l'affichage de la grille dans un fichier



Function Run(grilleInitiale : TypeGrille; n : Integer) : TypeGrille;
	Var i : Integer;
	Begin	

		ClrScr;
		If n < 0 Then Begin
			Repeat
				grilleInitiale := CalculerNouvelleGrille(grilleInitiale);	
				Delay(100);
				ClrScr;
				AfficherGrille(grilleInitiale);
				WriteLn(CompteCellule(grilleInitiale), ' cellules vivantes.');
			Until KeyPressed;
		End
		Else Begin
			AfficherGrille(grilleInitiale);
			Delay(1000);
			ClrScr;
			For i:=1 To n Do
				grilleInitiale := CalculerNouvelleGrille(grilleInitiale);
			AfficherGrille(grilleInitiale);
			WriteLn(CompteCellule(grilleInitiale), ' cellules vivantes.');
		End;
		Run := grilleInitiale;
	End;

End.

//Dans un premier temps, elle permet de faire un boucle infini tant qu'une touche n'est pas appuyée si n<0, et elle permet aussi d'afficher les différentes grilles, n fois, si n>0



