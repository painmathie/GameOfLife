Unit MoutonProg;
Interface
Uses Crt, Utils;

Function InitialiserGeneration(tableau2, tableau : TabPosition) : TypeGeneration;
Procedure AfficherGeneration(grille : TypeGeneration);
Procedure WriteGeneration(grille : TypeGeneration; Var  fic2 : Text);
Function Run(grille : TypeGeneration) : TypeGeneration;
Function RunGeneration(grille : TypeGeneration; nombreIteration : Integer) : TypeGeneration;
Function InitialiserGenerationRandom(nb, nb2 : Integer) : TypeGeneration;
Implementation
Function InitialiserGenerationRandom(nb, nb2 : Integer) : TypeGeneration;
	Var i, j, r, r2: Integer;
		grille : TypeGeneration;
		varHerbe : Herbe;
		varMouton : Mouton;
	Begin
		initialiserTableau(grille);
		For i := 0 To N-1 Do
			For j := 0 To N-1 Do Begin
				r := Random(100);
				r2 := Random(100);
				If r <= nb Then begin
					varHerbe.energie := HERBE_ENERGIE_INITIALE;
					varHerbe.age := 0;
					varHerbe.alive := True;
					grille[i][j].herbep := varHerbe;
				End;
				If r2 <= nb2 Then Begin
					varMouton.energie := MOUTON_ENERGIE_INITIALE;
					varMouton.age := 0;
					varMouton.alive := True;
					grille[i][j].moutonp := varMouton;
				End;
			end;
		InitialiserGenerationRandom := grille;
	End;
Function InitialiserGeneration(tableau2, tableau : TabPosition) : TypeGeneration;
	Var i, x, y : Integer;
		grille : TypeGeneration;
	Begin
		initialiserTableau(grille);
		For i := 1 To M Do Begin
			x := tableau[i].x;
			y := tableau[i].y;
			If (x <> -1) And (y <> -1) Then Begin
				grille[x][y].moutonp.energie := MOUTON_ENERGIE_INITIALE;
				grille[x][y].moutonp.alive := True;
			End;
		End;
		For i := 1 To M Do Begin
			x := tableau2[i].x;
			y := tableau2[i].y;
			If (x <> -1) And (y <> -1) Then Begin
				grille[x][y].herbep.energie := HERBE_ENERGIE_INITIALE;
				grille[x][y].herbep.alive := True;
			End;
		End;
		InitialiserGeneration := grille;
	End;

Procedure AfficherGeneration(grille : TypeGeneration);
	Var i,j : Integer;	
	Begin
		For i := 0 To N-1 Do Begin
			For j := 0 To N-1 Do Begin
				If grille[i][j].herbep.alive = True Then
					TextBackground(Green);
				If grille[i][j].moutonp.alive = False Then 
					Write ('  ')
				Else
					Write ('m ');	
				TextBackground(Black);
			End;
				WriteLn('|');
		End;
		For j := 0 To N-1 Do
			write('--');
		writeln('+');
	End;


Procedure WriteGeneration(grille : TypeGeneration; var  fic2 : Text);
	Var i,j : Integer;	
	Begin
		For i := 0 To N-1 Do Begin
			write(fic2, '#');
			For j := 0 To N-1 Do Begin
				If grille[i][j].herbep.alive = False Then 
					Write (fic2, '-')
				Else
					Write (fic2, 'h');
				If grille[i][j].moutonp.alive = False Then 
					Write (fic2, '- ')
				Else
					Write (fic2, 'm ');	
			End;
			Writeln (fic2, '');
		End;
		
		saveGrille(fic2, grille);
	End;

Function Run(grille : TypeGeneration) : TypeGeneration;
	Var i, j : integer;
		herbe2: Herbe;
		mouton2: Mouton;
		grille2 : typeGeneration;
		typePosition2 : TypePosition;
	Begin
		initialiserTableau(grille2);
		For i := 0 To N-1 Do Begin
			For j := 0 To N-1 Do Begin
				herbe2 := grille[i][j].herbep;
				mouton2 := grille[i][j].moutonp;
				If herbe2.alive = True Then Begin
					If herbe2.age < HERBE_AGE_MAX Then Begin
						herbe2.age := herbe2.age+1;
						herbe2.energie :=herbe2.energie+HERBE_ENERGIE; 
						If herbe2.energie >= HERBE_ENERGIE_REPRODUCTION Then Begin
							reproductionherbe(i,j, herbe2,grille, grille2);
							herbe2.energie := herbe2.energie-HERBE_ENERGIE_REPRODUCTION;
						End;
					End
				Else Begin
					herbe2.age:=0;
					herbe2.energie:=0;
					herbe2.alive:= False;
				End;
				grille2[i][j].herbep:=herbe2;
			End;	
			If mouton2.alive = True Then Begin
				If (mouton2.age < MOUTON_AGE_MAX) And (mouton2.energie > 0) Then Begin
					mouton2.age := mouton2.age + 1;
					If herbe2.alive = True Then Begin
						herbe2.age:=0;
						herbe2.energie:=0;
						herbe2.alive:= False;
						mouton2.energie := mouton2.energie+MOUTON_ENERGIE;
						grille2[i][j].herbep:=herbe2;
					End
					Else If mouton2.energie >= MOUTON_ENERGIE_REPRODUCTION Then Begin
						reproductionmouton(i,j,mouton2,grille,grille2);
						mouton2.energie := mouton2.energie-MOUTON_ENERGIE_REPRODUCTION ;
					End
					Else Begin
						typePosition2 := priomouton(i,j,mouton2,grille, grille2);
						If (typePosition2.x = -1) Or (typePosition2.y = -1) Then
							typePosition2 := caselibremouton(i,j,mouton2,grille, grille2);
							If (typePosition2.x <> -1) And (typePosition2.y <> -1) Then Begin
								mouton2.energie := mouton2.energie-2;
								grille2[typePosition2.x][typePosition2.y].moutonp:=mouton2;
								mouton2.age:=0;
								mouton2.energie:=0;
								mouton2.alive:= False;
							End
							Else
								mouton2.energie := mouton2.energie-1;
						End
					End
					Else Begin
						mouton2.age:=0;
						mouton2.energie:=0;
						mouton2.alive:= False;
					End;
					grille2[i][j].moutonp:=mouton2;
				End;				 
			End;
		End;
		Run := grille2;
	End;



Procedure CompteMoutonHerbe(grille : TypeGeneration; Var x, y : Integer);
Var i,j: Integer;
Begin
	x := 0;
	y := 0;
	For i := 0 To N-1 Do
		For j := 0 To N-1 Do Begin
			If grille[i][j].herbep.alive Then
				x := x + 1;
			If grille[i][j].moutonp.alive Then
				y := y + 1;
			End;
End;

Function RunGeneration(grille : TypeGeneration; nombreIteration : Integer) : TypeGeneration;
	Var i, x, y : integer;
	Begin
		ClrScr;
		If nombreIteration > 0 Then begin
			AfficherGeneration(grille);
			Delay(1000);
			For i := 1 To nombreIteration Do Begin
				grille := Run(grille);
				ClrScr;
			End;
			AfficherGeneration(grille);
			CompteMoutonHerbe(grille, x, y);
			WriteLn(x, ' herbes vivantes.');
			WriteLn(y, ' moutons vivants.');
		end
		Else Begin
			Repeat
				grille := Run(grille);
				Delay(100);
				ClrScr;
				AfficherGeneration(grille);
				CompteMoutonHerbe(grille, x, y);
				WriteLn(x, ' herbes vivantes.');
				WriteLn(y, ' moutons vivants.');
			Until KeyPressed;
		End;
		RunGeneration := grille
	End;
End.
