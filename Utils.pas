unit Utils;
interface
// Unit permet de mettre a disposition des variables et des fonctions pour d'autres programmes
Const N = 30;
Const M = 1000;
Const HERBE_ENERGIE_INITIALE = 1;
Const HERBE_AGE_MAX = 5;
Const HERBE_ENERGIE_REPRODUCTION = 10;
Const HERBE_ENERGIE = 4;
Const MOUTON_ENERGIE_INITIALE = 11;
Const MOUTON_AGE_MAX = 15;
Const MOUTON_ENERGIE_REPRODUCTION = 20;
Const MOUTON_ENERGIE = 14;
Const LOUP_ENERGIE_INITIALE = 15;
Const LOUP_AGE_MAX = 17;
Const LOUP_ENERGIE_REPRODUCTION = 32;
Const LOUP_ENERGIE = 30;

Type Herbe = Record
	energie, age : Integer;
	alive : Boolean;
	End;
Type Mouton = Record
	energie, age : Integer;
	alive : Boolean;
	End;
Type Loup = Record
	energie, age : Integer;
	alive : Boolean;
	End;
Type TypePosition = Record
	x, y : Integer;
	End;
type Position = Record
	herbep : Herbe;
	moutonp : Mouton;
	loupp : Loup;
	End;
Type TypeGeneration = Array[0..N-1,0..N-1] Of Position;
Type TabPosition = Array[1..M] Of TypePosition;


Procedure reproductionherbe(x, y : Integer; herbe2 : Herbe; grille : TypeGeneration; var grille2 : TypeGeneration);
Procedure initialiserTableau(var grille : TypeGeneration);
Function caseLibreMouton(x, y : Integer; mouton2 : Mouton; grille, grille2 : TypeGeneration) : TypePosition;
Function prioMouton(x, y : Integer; mouton2 : Mouton; grille, grille2 : TypeGeneration) : TypePosition;
Procedure reproductionMouton(x, y : Integer; mouton2 : Mouton; grille : TypeGeneration; Var grille2 : TypeGeneration);
Procedure initManuel(var listpos : TabPosition);
Procedure saveGrille(Var fic2 : Text; grille : TypeGeneration);
// Par exemple cette procédure est initialisé pour difre qu'elle st utilisable dans d'autre programmes
implementation

Procedure saveGrille(Var fic2 : Text; grille : TypeGeneration);
Var i, j, r : Integer;
Begin
	r := 0;
	WriteLn(fic2, '');
	Write(fic2, '$');
	For i := 0 To N-1 Do Begin
		For j := 0 To N-1 Do Begin
			Write (fic2, grille[i][j].herbep.alive);
			Write(fic2, ';');
			Write (fic2, grille[i][j].herbep.age);
			Write(fic2, ';');
			Write (fic2, grille[i][j].herbep.energie);
			Write(fic2, ':');
			Write (fic2, grille[i][j].moutonp.alive);
			Write(fic2, ';');
			Write (fic2, grille[i][j].moutonp.age);
			Write(fic2, ';');
			Write (fic2, grille[i][j].moutonp.energie);
			Write(fic2, ':');
			Write (fic2, grille[i][j].loupp.alive);
			Write(fic2, ';');
			Write (fic2, grille[i][j].loupp.age);
			Write(fic2, ';');
			Write (fic2, grille[i][j].loupp.energie);
			Write(fic2, '*');
			r := r + 1;
			If r >= 6 Then Begin
			Writeln(fic2, '');
			r := 0;
			End;
		End;
	End;
	Write(fic2, '$');
End;
// Ici on dit donc, comment elle va être utilisé
Procedure initManuel(var listpos : TabPosition);
	Var varTypePosition : TypePosition;
		nbcn, x, y, i : Integer;
		isok : Boolean;
	Begin
		ReadLn(nbcn);
		For i:=1 To nbcn Do Begin
			isok := True;
			While isok Do Begin
				WriteLn('Saisir l abscisse :');
				ReadLn(x);
				WriteLn('Saisir l ordonné :');
				ReadLn(y);
				If ((x<0) Or (y<0) Or (x>=N) Or (y>=N)) Then 
					WriteLn('Une erreur dans les coordonnées !')
				Else Begin
					varTypePosition.x := x;
					varTypePosition.y := y;
					listpos[i] := varTypePosition;
					isok := False;
				End;
			End;
		End;
	End;
// Cette procédure nous permet donc d'entrer les cases du tableau que nous voulons, de taille nbcn rentrée par l'utilisateur. Si il y a des erreurs, on revient avant, et la machine demande de les corriger.
// Elle ne retourne rien, car elle modifie l'argument(le tableau)



Procedure initialiserTableau(var grille : TypeGeneration);
	Var i, j : Integer;
		varHerbe : Herbe;
		varMouton : Mouton;
		varLoup : Loup;
		varPosition : Position;
	Begin
	For i := 0 To N-1 Do Begin
			For j := 0 To N-1 Do Begin
				varHerbe.energie := 0;
				varHerbe.age := 0;
				varHerbe.alive := False;
				varMouton.energie := 0;
				varMouton.age := 0;
				varMouton.alive := False;
				varLoup.energie := 0;
				varLoup.age := 0;
				varLoup.alive := False;
				varPosition.herbep := varHerbe;
				varPosition.moutonp := varMouton;
				varPosition.loupp := varLoup;
				grille[i][j] := varPosition;
			End;
	End;
	End;
// ici on met tout a 0

Function caseLibreMouton(x, y : Integer; mouton2 : Mouton; grille, grille2 : TypeGeneration) : TypePosition;
	Var i,j, x2, y2, c : Integer;
		tabPosition2 : tabPosition;
		typePosition2 : TypePosition;
	Begin
		c := 0;
		For i := x-1 To x+1 Do Begin
			If i = -1 Then x2 := N-1 Else If i = N Then x2 := 0 Else x2 := i;
			If (grille[x2][y].moutonp.alive = False) and (grille2[x2][y].moutonp.alive = False) Then Begin
				typePosition2.x := x2;
				typePosition2.y := y;
				tabPosition2[c] := typePosition2;
				c := c + 1;
			End;
		End;
		For j := y-1 To y+1 Do Begin
			If j = -1 Then y2 := N-1 Else If j = N Then y2 := 0 Else y2 := j;
			If (grille[x][y2].moutonp.alive = False) and (grille2[x][y2].moutonp.alive = False) Then Begin
				typePosition2.x := x;
				typePosition2.y := y2;
				tabPosition2[c] := typePosition2;
				c := c + 1;
// Le c au dessus, permet de selectionner et de compter le nombre de cases libres
			End;			
		End;
		If c = 0 Then Begin
			typePosition2.x := -1;
			typePosition2.y := -1;
			exit(typePosition2) ;
//si il est a 0, le mouton n'a pas de possibilité de bouger (pas de case libre)
		End;
		c := random(c);
		exit(tabPosition2[c]);
//sinon, le c permet un random entre les cases dispos, et la fonction renvoi donc la case où le mouton à desormais bougé
	End;

Function prioMouton(x, y : Integer; mouton2 : Mouton; grille, grille2 : TypeGeneration) : TypePosition;
	Var i,j, x2, y2, c : Integer;
		tabPosition2 : TabPosition;
		typePosition2 : TypePosition;
	Begin
		c := 0;
		For i := x-1 To x+1 Do Begin
			If i = -1 Then x2 := N-1 Else If i = N Then x2 := 0 Else x2 := i;
			If (grille[x2][y].moutonp.alive = False) And (grille2[x2][y].moutonp.alive = False) And (grille[x2][y].herbep.alive = True) Then Begin
				typePosition2.x := x2;
				typePosition2.y := y;
				tabPosition2[c] := typePosition2;
				c := c + 1;
			End;
		End;
		For j := y-1 To y+1 Do Begin
			If j = -1 Then y2 := N-1 Else If j = N Then y2 := 0 Else y2 := j;
			If (grille[x][y2].moutonp.alive = False) And (grille2[x][y2].moutonp.alive = False) And (grille[x][y2].herbep.alive = True) Then Begin
				typePosition2.x := x;
				typePosition2.y := y2;
				tabPosition2[c] := typePosition2;
				c := c + 1;
			End;			
		End;
		If c = 0 Then Begin
			typePosition2.x := -1;
			typePosition2.y := -1;
			exit(typePosition2) ;
		End;
		c := random(c);
		exit(tabPosition2[c]);
//Cette fonction est semblable a la précédente, sauf qu'ici il est demandé qu'il y ai de l'herbe pour le mouvement de la bête poilue.
	End;

Procedure reproductionherbe(x, y : Integer; herbe2 : Herbe; grille : TypeGeneration; var grille2 : TypeGeneration);
	Var i,j, x2, y2 : Integer;
	Begin
		For i := x-1 to x+1 do Begin
			If i = -1 Then
				x2 := N-1 
			Else If i = N Then
				x2 := 0
			Else
				x2 := i;		
			For j := y-1 to y+1 do Begin
				If j = -1 Then
					y2 := N-1
				Else If j = N Then
					y2 := 0
				Else
					y2 := j;
				If ((x <> x2) Or (y <> y2)) and (grille[x2][y2].herbep.alive = False) and (grille2[x2][y2].herbep.alive = False) Then Begin
					grille2[x2][y2].herbep.alive := True;
					grille2[x2][y2].herbep.energie := HERBE_ENERGIE_INITIALE;
				End;
			End;
		End;
	End;
//Cette fonction permet de simuler l'extention de l'herbe. elle décrit également le monde "torique"


Procedure reproductionMouton(x, y : Integer; mouton2 : Mouton; grille : TypeGeneration; Var grille2 : TypeGeneration);
	Var typePosition2 : TypePosition;
	Begin
		typePosition2 := caseLibreMouton(x, y, mouton2, grille, grille2);
		If (typePosition2.x <> -1) And (typePosition2.y <> -1) Then Begin
			grille2[typePosition2.x][typePosition2.y].moutonp.alive := True;
			grille2[typePosition2.x][typePosition2.y].moutonp.energie := MOUTON_ENERGIE_INITIALE;
		End;
	End;
end.
//reproduction mouton
